package com.eladiut.honey_i_shrunk_the_space.blocks;

import com.eladiut.honey_i_shrunk_the_space.util.MyVector3D;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class ShrunkSpaceBase extends Block {
    public MyVector3D size;
    public ShrunkSpaceBase() {
        super(
                Properties.create(Material.ROCK)
                        .hardnessAndResistance(6f,24f)
                        .sound(SoundType.STONE)
                        .harvestLevel(1)
                        .harvestTool(ToolType.PICKAXE)
        );
    }

    public ShrunkSpaceBase(MyVector3D size) {
        super(
                Properties.create(Material.ROCK)
                .hardnessAndResistance(6f,24f)
                .sound(SoundType.STONE)
                .harvestLevel(1)
                .harvestTool(ToolType.PICKAXE)
        );
        this.size = size;
    }

}
