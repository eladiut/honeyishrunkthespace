package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachine;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;

public class ShrunkSpaceBuilder extends AbstractMachine {

    public ShrunkSpaceBuilder() {
        super(Properties
                .create(Material.IRON)
                .lightValue(6));
    }

    @Override
    public BooleanProperty propertyToUseAsLighting() {
        return BlockStateProperties.POWERED;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) { return new ShrunkSpaceBuilderTileEntity(); }
}
