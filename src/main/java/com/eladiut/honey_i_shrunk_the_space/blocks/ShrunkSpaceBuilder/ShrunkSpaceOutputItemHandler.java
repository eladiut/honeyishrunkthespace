package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers.OutputSlotItemHandler;
import net.minecraft.item.Item;
import net.minecraftforge.items.IItemHandler;

public class ShrunkSpaceOutputItemHandler extends OutputSlotItemHandler {
    public ShrunkSpaceOutputItemHandler(IItemHandler itemHandler, MachineSlotsEnum slot) { super(itemHandler, slot); }

    @Override
    public Item getOutputItem() {
        return RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.SHRUNK_SPACE);
    }
}
