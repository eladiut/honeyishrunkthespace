package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;

public enum ShrunkBuilderSlots implements MachineSlotsEnum {
    UP("up",0, 20, 14),
    DOWN("down",1, 20, 53),
    NORTH("north",2, 72, 14),
    SOUTH("south",3, 72, 53),
    EAST("east",4, 91, 33),
    WEST("west",5, 53, 33),
    OUTPUT("result",6, 143, 33);

    private final String text;
    private final int slotNum;
    private final int xPosition;
    private final int yPosition;
    ShrunkBuilderSlots(String name, int slot, int xPosition, int yPosition) {
        this.text = name;
        this.slotNum = slot;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    };

    @Override
    public String toString() {
        return text;
    }

    @Override
    public int getSlotNum() { return this.slotNum; }

    @Override
    public int getxPosition() {
        return xPosition;
    }

    @Override
    public int getyPosition() {
        return yPosition;
    }

}
