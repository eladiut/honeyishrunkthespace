package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.IAbstractMachineTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.BlockItemBase;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBase;
import com.eladiut.honey_i_shrunk_the_space.blocks.Walls.IWallBlock;
import com.eladiut.honey_i_shrunk_the_space.blocks.Walls.WallBlockHandler;
import com.eladiut.honey_i_shrunk_the_space.blocks.Walls.WallBlockItemBase;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.MachineEnergyStorage;
import com.eladiut.honey_i_shrunk_the_space.util.MyVector3D;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nullable;

import static com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderContainer.isWallBlock;
import static com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler.*;

public class ShrunkSpaceBuilderTileEntity extends AbstractMachineTileEntity implements IAbstractMachineTileEntity {
    public static int ENERGY_PER_TICK_USAGE = 60;
    public static double PROCESSING_SPEED_MULT = 0.9;

    public ShrunkSpaceBuilderTileEntity() {
        super(SHRUNK_SPACE_BUILDER_TILE_ENTITY.get(), 7);
    }

    @Override
    public int getSlotNum() {
        return 7;
    }

    @Override
    public void Tick() {
        if (world.isRemote) {
            return;
        }

        setBlockState();

        if(isProcessing) {
            if(this.processingTime == 0) {
                getItemHandler().ifPresent(h -> {
                    h.insertItem(ShrunkBuilderSlots.OUTPUT.getSlotNum(), resultStack, false);
                    this.isProcessing = false;
                });
            }
            else {
                getEnergyHandler().ifPresent(e -> {
                    if(e.getEnergyStored() > 0) {
                        e.extractEnergy(ENERGY_PER_TICK_USAGE, false);
                        this.processingTime--;
                    }
                });
            }
        } else {
            getItemHandler().ifPresent(h -> {
                if (isRecipeValid(h)) {
                    MyVector3D size = getSpaceSize(h);
                    double volume = size.getVolume();
                    shrinkValidItemStacks(h);

                    this.processingTime = this.initialProcessingTime = (int)(volume * 100 * (1-PROCESSING_SPEED_MULT));
                    this.resultStack = new ItemStack(BLOCK_ITEM_SET.getItem(RegisteredObject.SHRUNK_SPACE));
                    CompoundNBT nbt = new CompoundNBT();
                    nbt.put("size", size.serializeNBT());
                    this.resultStack.setTag(nbt);

                    this.isProcessing = true;
                }
            });
        }
    }

    @Override
    public void read(CompoundNBT tag) {
        super.read(tag);
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        return super.write(tag);
    }

    @Override
    public ITextComponent getDisplayName() {
        return new StringTextComponent(RegisteredObject.SHRUNK_SPACE_BUILDER.toString());
    }

    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new ShrunkSpaceBuilderContainer(windowId, this, pos, playerInventory, playerEntity);
    }

    private void setBlockState() {
        int energy = getCapability(CapabilityEnergy.ENERGY).map(h -> ((MachineEnergyStorage)h).getEnergyStored()).orElse(0);
        BlockState blockState = world.getBlockState(pos);
        if(blockState.has(BlockStateProperties.POWERED)) {
            if (blockState.get(BlockStateProperties.POWERED) != energy > 0) {
                world.setBlockState(pos, blockState.with(BlockStateProperties.POWERED, energy > 0), 3);
            }
        }
    }

    private boolean isRecipeValid(IItemHandler handler) {
        ItemStack upStack = handler.getStackInSlot(ShrunkBuilderSlots.UP.getSlotNum());
        ItemStack downStack = handler.getStackInSlot(ShrunkBuilderSlots.DOWN.getSlotNum());
        ItemStack northStack = handler.getStackInSlot(ShrunkBuilderSlots.NORTH.getSlotNum());
        ItemStack southStack = handler.getStackInSlot(ShrunkBuilderSlots.SOUTH.getSlotNum());
        ItemStack eastStack = handler.getStackInSlot(ShrunkBuilderSlots.EAST.getSlotNum());
        ItemStack westStack = handler.getStackInSlot(ShrunkBuilderSlots.WEST.getSlotNum());

        return isWallBlock(westStack) &&
                isWallBlock(eastStack) &&
                isWallBlock(northStack) &&
                isWallBlock(southStack) &&
                isWallBlock(upStack) &&
                isWallBlock(downStack) &&
                westStack.getItem() == eastStack.getItem() &&
                northStack.getItem() == southStack.getItem();
    }

    private void shrinkValidItemStacks(IItemHandler handler) {
        handler.getStackInSlot(ShrunkBuilderSlots.UP.getSlotNum()).shrink(1);
        handler.getStackInSlot(ShrunkBuilderSlots.DOWN.getSlotNum()).shrink(1);
        handler.getStackInSlot(ShrunkBuilderSlots.NORTH.getSlotNum()).shrink(1);
        handler.getStackInSlot(ShrunkBuilderSlots.SOUTH.getSlotNum()).shrink(1);
        handler.getStackInSlot(ShrunkBuilderSlots.EAST.getSlotNum()).shrink(1);
        handler.getStackInSlot(ShrunkBuilderSlots.WEST.getSlotNum()).shrink(1);
    }

    private MyVector3D getSpaceSize(IItemHandler handler) {
        int upBlock = WallBlockHandler.getWallValue(handler.getStackInSlot(ShrunkBuilderSlots.UP.getSlotNum()).getItem());
        int downBlock = WallBlockHandler.getWallValue(handler.getStackInSlot(ShrunkBuilderSlots.DOWN.getSlotNum()).getItem());
        int northBlock = WallBlockHandler.getWallValue(handler.getStackInSlot(ShrunkBuilderSlots.NORTH.getSlotNum()).getItem());
        int eastBlock = WallBlockHandler.getWallValue(handler.getStackInSlot(ShrunkBuilderSlots.EAST.getSlotNum()).getItem());

        return new MyVector3D(upBlock + downBlock, northBlock, eastBlock);
    }


}
