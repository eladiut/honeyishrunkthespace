package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineContainer;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers.WallSlotItemHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nonnull;

public class ShrunkSpaceBuilderContainer extends AbstractMachineContainer {

    public ShrunkSpaceBuilderContainer(int windowId, BlockPos pos, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        this(windowId, new ShrunkSpaceBuilderTileEntity(), pos, playerInventory, playerEntity);
    }

    public ShrunkSpaceBuilderContainer(int windowId, ShrunkSpaceBuilderTileEntity tileEntity, BlockPos pos, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        super(
                windowId,
                tileEntity,
                pos,
                playerEntity,
                RegistryHandler.SHRUNK_SPACE_BUILDER_CONTAINER.get(),
                RegisteredObject.SHRUNK_SPACE_BUILDER);
        super.Init(playerInventory);
    }

    @Override
    protected void addMachineSlots(TileEntity tileEntity) {
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
            slotsMap.put(ShrunkBuilderSlots.UP.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.UP));
            slotsMap.put(ShrunkBuilderSlots.DOWN.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.DOWN));
            slotsMap.put(ShrunkBuilderSlots.NORTH.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.NORTH));
            slotsMap.put(ShrunkBuilderSlots.SOUTH.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.SOUTH));
            slotsMap.put(ShrunkBuilderSlots.EAST.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.EAST));
            slotsMap.put(ShrunkBuilderSlots.WEST.toString(), new WallSlotItemHandler(h, ShrunkBuilderSlots.WEST));
            slotsMap.put(ShrunkBuilderSlots.OUTPUT.toString(), new ShrunkSpaceOutputItemHandler(h, ShrunkBuilderSlots.OUTPUT));

            slotsMap.forEach((key, value) -> {
                addSlot(value);
            });
        });
    }

    @Override
    protected InvWrapper getPlayerInventory(PlayerInventory playerInventory) {
        return new InvWrapper(playerInventory) {
            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if(stack != playerInventory.getStackInSlot(slot) && slot == ShrunkBuilderSlots.OUTPUT.getSlotNum()) {
                    return stack;
                }
                return super.insertItem(slot, stack, simulate);
            }
        };
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            itemstack = stack.copy();
            if (index >= 0 && index <= 6) {
                if (!this.mergeItemStack(stack, 2, 37, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(stack, itemstack);
            } else {
                if (isWallBlock(stack) && !this.mergeItemStack(stack, 0, 6, false)) {
                    return ItemStack.EMPTY;
                } else if (index < 28 && !this.mergeItemStack(stack, 28, 37, false)) {
                    return ItemStack.EMPTY;
                } else if (index < 37 && !this.mergeItemStack(stack, 1, 28, false)) {
                    return ItemStack.EMPTY;
                }
            }

            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }

            if (stack.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, stack);
        }

        return itemstack;
    }
    public static boolean isWallBlock(ItemStack stack) {
        return stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.WALL_BLOCK) ||
                stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.COMPRESSED_WALL_BLOCK) ||
                stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.DBL_COMPRESSED_WALL_BLOCK) ||
                stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.TRPL_COMPRESSED_WALL_BLOCK) ||
                stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.QDRPL_COMPRESSED_WALL_BLOCK);
    }
}
