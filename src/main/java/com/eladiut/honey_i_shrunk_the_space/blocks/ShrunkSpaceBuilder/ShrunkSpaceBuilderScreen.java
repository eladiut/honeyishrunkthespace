package com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineGuiTile;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineScreen;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class ShrunkSpaceBuilderScreen extends AbstractMachineScreen<ShrunkSpaceBuilderContainer> {

    private ResourceLocation GUI_TEXTURE = new ResourceLocation(HoneyIShrunkTheSpace.MOD_ID, "textures/gui/shrunkspacebuilder_gui.png");

    public ShrunkSpaceBuilderScreen(ShrunkSpaceBuilderContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        Minecraft.getInstance().fontRenderer.drawString("Space", 114,62,4210752);
        Minecraft.getInstance().fontRenderer.drawString("Shrinker", 108,71,4210752);
    }

    @Override
    protected ResourceLocation getGuiTexture() {
        return GUI_TEXTURE;
    }

    @Override
    protected boolean shouldDrawFlame() {
        return false;
    }

    @Override
    protected boolean shouldDrawEnergyBar() {
        return true;
    }

    @Override
    protected boolean shouldDrawShrinkerArrow() {
        return true;
    }

    @Override
    protected AbstractMachineGuiTile flameUnlitTile() {
        return null;
    }
}
