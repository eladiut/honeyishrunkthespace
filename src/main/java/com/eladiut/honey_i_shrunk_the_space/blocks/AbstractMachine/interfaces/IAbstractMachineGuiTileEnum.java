package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces;

public interface IAbstractMachineGuiTileEnum {
    public int posX();
    public int posY();
    public int sizeX();
    public int sizeY();
}
