package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderTileEntity;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.MachineEnergyStorage;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.IntReferenceHolder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import java.util.HashMap;

public abstract class AbstractMachineContainer extends Container {
    public AbstractMachineTileEntity tileEntity;
    protected PlayerEntity playerEntity;
    protected IItemHandler playerInventory;
    protected HashMap<String, SlotItemHandler> slotsMap = new HashMap<String, SlotItemHandler>();
    private RegisteredObject registeredObject = null;

    public AbstractMachineContainer(int windowId, AbstractMachineTileEntity tileEntity, BlockPos pos, PlayerEntity playerEntity, ContainerType<?> containerType, RegisteredObject registeredBlock) {
        super(containerType, windowId);
        this.tileEntity = tileEntity;
        this.playerEntity = playerEntity;
        this.registeredObject = registeredBlock;
    }

    public void Init(PlayerInventory playerInventory) {
        this.playerInventory = getPlayerInventory(playerInventory);
        addMachineSlots(tileEntity);
        layoutPlayerInventorySlots(8, 84);
        trackData();
    }


    public int getEnergy() {
        return tileEntity.getCapability(CapabilityEnergy.ENERGY).map(IEnergyStorage::getEnergyStored).orElse(0);
    }

    public int getMaxEnergyStored() {
        return tileEntity.getCapability(CapabilityEnergy.ENERGY).map(IEnergyStorage::getMaxEnergyStored).orElse(0);
    }


    public int getProcessingTime() {
        return tileEntity.getProcessingTime();
    }

    public int getInitialProcessingTime() {
        return tileEntity.getInitialProcessingTime();
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        if(this.registeredObject != null && RegistryHandler.BLOCK_SET.containsKey(this.registeredObject)) {
            return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerIn, RegistryHandler.BLOCK_SET.getBlock(registeredObject));
        } else {
            return true;
        }
    }

    protected InvWrapper getPlayerInventory(PlayerInventory playerInventory) {
        return null;
    }

    protected abstract void addMachineSlots(TileEntity tileEntity);

    @Override
    public abstract ItemStack transferStackInSlot(PlayerEntity playerIn, int index);

    protected void layoutPlayerInventorySlots(int leftCol, int topRow) {
        // player Inventory
        addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);

        // Hotbar
        topRow += 58;
        addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
    }
    private void trackData() {
        trackInt(new IntReferenceHolder() {
            @Override
            public int get() {
                return getEnergy();
            }

            @Override
            public void set(int value) {
                tileEntity.getCapability(CapabilityEnergy.ENERGY).ifPresent(h -> ((MachineEnergyStorage)h).setEnergy(value));
            }
        });
        trackInt(new IntReferenceHolder() {
            @Override
            public int get() {
                return getFuelLevel();
            }

            @Override
            public void set(int value) {
                tileEntity.setFuelLevel(value);
            }
        });
        trackInt(new IntReferenceHolder() {
            @Override
            public int get() {
                return getTotalBurnTime();
            }

            @Override
            public void set(int value) {
                tileEntity.setTotalBurnTime(value);
            }
        });
        trackInt(new IntReferenceHolder() {
            @Override
            public int get() {
                return tileEntity.getProcessingTime();
            }

            @Override
            public void set(int value) {
                tileEntity.setProcessingTime(value);
            }
        });
        trackInt(new IntReferenceHolder() {
            @Override
            public int get() {
                return tileEntity.getInitialProcessingTime();
            }

            @Override
            public void set(int value) {
                tileEntity.setInitialProcessingTime(value);
            }
        });
    }

    private int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
        for (int i = 0; i < amount; i++) {
            addSlot(new SlotItemHandler(handler, index, x, y));
            x += dx;
            index++;
        }
        return index;
    }

    private int addSlotBox(IItemHandler handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy) {
        for(int j = 0; j < verAmount; j++) {
            index = addSlotRange(handler, index, x, y, horAmount, dx);
            y += dy;
        }
        return index;
    }

    public boolean isBurning() {
        return tileEntity.isBurning();
    }

    public int getTotalBurnTime() {
        return tileEntity.getTotalBurnTime();
    }

    public int getFuelLevel() {
        return tileEntity.getFuelLevel();
    }
}
