package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.text.ITextComponent;

public interface IAbstractMachineTileEntity {
    public int getMaxEnergyStored();
    public int getMaxTransferRate();
    public int getSlotNum();
    public ITextComponent getDisplayName();
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity);
}
