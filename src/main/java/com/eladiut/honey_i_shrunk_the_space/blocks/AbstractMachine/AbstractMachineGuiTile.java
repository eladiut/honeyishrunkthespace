package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.IAbstractMachineGuiTileEnum;

public enum AbstractMachineGuiTile implements IAbstractMachineGuiTileEnum {
    LIT_FIRE(176, 0, 14, 14),
    LIT_ARROW(176, 14, 22, 16),
    LIT_ELECTRICITY(176, 31, 4, 50),
    UNLIT_ELECTRICITY(163, 16, 4, 50),
    SPACE_SHRINKER_UNLIT_ARROW(117, 34, 22, 16),
    GENERATOR_UNLIT_FIRE(58, 37, 13, 13),
    ;
    int posX = 0;
    int posY = 0;
    int sizeX = 0;
    int sizeY = 0;
    AbstractMachineGuiTile(int posX, int posY, int sizeX, int sizeY) {
        this.posX = posX;
        this.posY = posY;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }
    @Override
    public int posX() {
        return this.posX;
    }

    @Override
    public int posY() {
        return this.posY;
    }

    @Override
    public int sizeX() {
        return this.sizeX;
    }

    @Override
    public int sizeY() {
        return this.sizeY;
    }
}
