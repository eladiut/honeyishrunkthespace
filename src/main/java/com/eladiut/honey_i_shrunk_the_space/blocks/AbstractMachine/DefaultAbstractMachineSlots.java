package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;

public enum DefaultAbstractMachineSlots implements MachineSlotsEnum {
    INPUT("input",0, 20, 14),
    OUTPUT("output",1, 143, 33);

    private final String text;
    private final int slotNum;
    private final int xPosition;
    private final int yPosition;

    DefaultAbstractMachineSlots(String name, int slot, int xPosition, int yPosition) { this.text = name; this.slotNum = slot;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    };

    @Override
    public String toString() {
        return text;
    }
    public int getSlotNum() { return this.slotNum; }

    @Override
    public int getxPosition() {
        return this.xPosition;
    }

    @Override
    public int getyPosition() {
        return this.yPosition;
    }

}
