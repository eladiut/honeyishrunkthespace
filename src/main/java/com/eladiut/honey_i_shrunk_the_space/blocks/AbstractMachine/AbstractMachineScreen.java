package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public abstract class AbstractMachineScreen<T extends AbstractMachineContainer> extends ContainerScreen<T> {

    public AbstractMachineScreen(T screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected abstract void drawGuiContainerForegroundLayer(int mouseX, int mouseY);

    protected abstract ResourceLocation getGuiTexture();

    protected abstract boolean shouldDrawFlame();

    protected abstract boolean shouldDrawEnergyBar();

    protected abstract boolean shouldDrawShrinkerArrow();

    protected abstract AbstractMachineGuiTile flameUnlitTile();

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    public void drawString(FontRenderer fontRenderer, String text, int locX, int locY, int color) {
        fontRenderer.drawStringWithShadow(text, (float)locX, (float)locY, color);
    }
    /**
     * Draws the background layer of this container (behind the items).
     */
    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(getGuiTexture());
        int relX = (this.width - this.xSize) / 2;
        int relY = (this.height - this.ySize) / 2;
        this.blit(relX, relY, 0, 0, this.xSize, this.ySize);
        if(shouldDrawFlame()) {
            this.drawFlame(flameUnlitTile());
        }
        if(shouldDrawEnergyBar()) {
            this.drawEnergyBar();
        }
        if(shouldDrawShrinkerArrow()) {
            this.drawShrinkerArrow();
        }
    }

    @Override
    protected void renderHoveredToolTip(int mouseX, int mouseY) {
        if (isPointInRegion(162, 19, 6, 52, mouseX, mouseY)) {
            String text = container.getEnergy() + "/" + container.getMaxEnergyStored();
            renderTooltip(text, mouseX, mouseY);
        }
        super.renderHoveredToolTip(mouseX, mouseY);
    }



    protected void fireVerticalBlit(AbstractMachineGuiTile unlit_tile, AbstractMachineGuiTile lit_tile, int currentHeight) {
        int guiX = (this.width - this.xSize) / 2;
        int guiY = (this.height - this.ySize) / 2;
        blit(
                guiX + unlit_tile.posX(),
                guiY + unlit_tile.posY() + (unlit_tile.sizeY() - 1) - currentHeight,
                        lit_tile.posX(),
                (unlit_tile.sizeY() - 1) - currentHeight,
                        lit_tile.sizeX(),
                currentHeight + 1);
    }

    protected void verticalBlit(AbstractMachineGuiTile unlit_tile, AbstractMachineGuiTile lit_tile, int currentHeight) {
        int guiX = (this.width - this.xSize) / 2;
        int guiY = (this.height - this.ySize) / 2;
        blit(
                guiX + unlit_tile.posX(),
                guiY + (unlit_tile.posY() + unlit_tile.sizeY()) - currentHeight,
                        lit_tile.posX(),
                        lit_tile.posY(),
                        lit_tile.sizeX(),
                        currentHeight
        );
    }

    protected void horizontalBlit(AbstractMachineGuiTile unlit_tile, AbstractMachineGuiTile lit_tile, int currentWidth) {
        int guiX = (this.width - this.xSize) / 2;
        int guiY = (this.height - this.ySize) / 2;
        blit(
                guiX + unlit_tile.posX(),
                guiY + unlit_tile.posY(),
                lit_tile.posX(),
                lit_tile.posY(),
                currentWidth,
                lit_tile.sizeY()
        );
    }

    protected void drawEnergyBar() {
        // Energy
        if(container.tileEntity != null) {
            int energy = getEnergyIconHeight();
            verticalBlit(AbstractMachineGuiTile.UNLIT_ELECTRICITY, AbstractMachineGuiTile.LIT_ELECTRICITY, energy);
        }
    }

    protected void drawShrinkerArrow() {
        // Processing arrow
        if(container.tileEntity != null) {
            int processing = this.getProcessingProgression();
            horizontalBlit(AbstractMachineGuiTile.SPACE_SHRINKER_UNLIT_ARROW, AbstractMachineGuiTile.LIT_ARROW, processing);
        }
    }

    protected void drawFlame(AbstractMachineGuiTile unlit_tile) {
        // Fuel remaining
        if(container.tileEntity != null) {
            if (container.isBurning()) {
                int height = getFlameIconHeight();
                fireVerticalBlit(unlit_tile, AbstractMachineGuiTile.LIT_FIRE, height);
            }
        }
    }

    private int getFlameIconHeight() {
        int total = container.getTotalBurnTime();
        if (total == 0) total = 200;
        return container.getFuelLevel() * 13 / total;
    }

    private int getEnergyIconHeight() {
        double total = container.getMaxEnergyStored();
        if (total == 0) total = 200;
        return (int)(((double)container.getEnergy() / total) * (double)AbstractMachineGuiTile.LIT_ELECTRICITY.sizeY());
    }

    private int getProcessingProgression() {
            int i = container.getProcessingTime();
            int j = container.getInitialProcessingTime();
            return (j != 0 && i != 0) ? (j - i) * AbstractMachineGuiTile.SPACE_SHRINKER_UNLIT_ARROW.sizeX() / j : 0;

    }

}
