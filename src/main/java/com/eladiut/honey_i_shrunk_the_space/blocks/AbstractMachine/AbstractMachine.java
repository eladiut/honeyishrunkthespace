package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class AbstractMachine extends Block {

    public AbstractMachine(Block.Properties properties) {
        super(
                properties
                .sound(SoundType.METAL)
                .hardnessAndResistance(6f,24f)
                .harvestLevel(2)
                .harvestTool(ToolType.PICKAXE)
        );
    }

    public AbstractMachine() {
        super(
            Properties
                .create(Material.IRON)
                .sound(SoundType.METAL)
                .hardnessAndResistance(6f,24f)
                .harvestLevel(2)
                .harvestTool(ToolType.PICKAXE)
        );
    }


    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        if(placer != null) {
            worldIn.setBlockState(pos, state.with(BlockStateProperties.HORIZONTAL_FACING, getFacingFromEntity(pos, placer)), 2);
        }
    }

    public static Direction getFacingFromEntity(BlockPos clickedBlock, LivingEntity entity) {
        return Direction.getFacingFromVector(
                (float)(entity.getPosition().getX() - clickedBlock.getX()),
                0f,
                (float)(entity.getPosition().getZ() - clickedBlock.getZ()));
    }

    public abstract BooleanProperty propertyToUseAsLighting();

    @Override
    public int getLightValue(BlockState state) {
        BooleanProperty prop = propertyToUseAsLighting();
        if(state.has(prop)) {
            return state.get(prop) ? super.getLightValue(state) : 0;
        } else {
            return 0;
        }
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    public abstract TileEntity createTileEntity(BlockState state, IBlockReader world);

    @Override
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult trace) {
        if (!world.isRemote) {
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity instanceof INamedContainerProvider) {
                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) tileEntity, tileEntity.getPos());
            } else {
                throw new IllegalStateException("Our named container provider is missing!");
            }
            return ActionResultType.SUCCESS;
        }
        return super.onBlockActivated(state, world, pos, player, hand, trace);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(
                BlockStateProperties.HORIZONTAL_FACING,
                BlockStateProperties.LIT,
                BlockStateProperties.POWERED
        );
    }
}
