package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces;

public interface MachineSlotsEnum {
    public String toString();
    public int getSlotNum();
    public int getxPosition();
    public int getyPosition();
}
