package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine;

import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.MachineEnergyStorage;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelDataManager;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

public abstract class AbstractMachineTileEntity extends TileEntity implements ITickableTileEntity, INamedContainerProvider {
    private final int DEFAULT_MAX_ENERGY = 10_000;
    private final int DEFAULT_TRANSFER_RATE = 500;
    private int fuelLevel;
    private int initialFuelLevel;
    private boolean markedAsDirty = false;
    private int SlotNum;
    public int processingTime = 0;
    public int initialProcessingTime = 0;
    public boolean isProcessing = false;
    public ItemStack resultStack = null;
    private LazyOptional<IItemHandler> itemHandler = LazyOptional.of(this::createHandler);
    private LazyOptional<MachineEnergyStorage> energyHandler = LazyOptional.of(this::createEnergy);

    public AbstractMachineTileEntity(TileEntityType<?> tileEntityTypeIn, int slotNum) {
        super(tileEntityTypeIn);
        this.SlotNum = slotNum;
    }

    public boolean isBurning() {
        return fuelLevel > 0;
    }

    public void setMarkedAsDirty(boolean markedAsDirty) {
        this.markedAsDirty = markedAsDirty;
    }

    public LazyOptional<MachineEnergyStorage> getEnergyHandler() {
        return energyHandler;
    }

    public LazyOptional<IItemHandler> getItemHandler() {
        return itemHandler;
    }

    public boolean isMarkedAsDirty() {
        return markedAsDirty;
    }

    public void setSlotNum(int slotNum) {
        SlotNum = slotNum;
    }

    public int setInitialFuelLevel(int initialFuelLevel) {
        return this.initialFuelLevel = initialFuelLevel;
    }

    public int setFuelLevel(int fuelLevel) {
        return this.fuelLevel = fuelLevel;
    }

    public int getFuelLevel() {
        return this.fuelLevel;
    }

    public int getTotalBurnTime() {
        return this.initialFuelLevel;
    }

    public int setTotalBurnTime(int value) {
        return this.initialFuelLevel = value;
    }

    public abstract void Tick();

    @Override
    public void tick() {
        this.Tick();
    }

    public void markDirty() {
        this.getWorld().notifyBlockUpdate(this.pos, this.getBlockState(), this.getBlockState(), Constants.BlockFlags.BLOCK_UPDATE);
        if(!markedAsDirty) {
            super.markDirty();
        }
    }
    @Override
    public void read(CompoundNBT tag) {
        super.read(tag);
        if(tag.contains("fuel")) {
            fuelLevel = tag.getInt("fuel");
        }
        if(tag.contains("initialFuelLevel")) {
            initialFuelLevel = tag.getInt("initialFuelLevel");
        }
        if(tag.contains("inv")) {
            CompoundNBT invTag = tag.getCompound("inv");
            itemHandler.ifPresent(h -> ((INBTSerializable<CompoundNBT>)h).deserializeNBT(invTag));
        }
        if(tag.contains("energy")) {
            energyHandler.ifPresent(h -> h.setEnergy(tag.getCompound("energy").getInt("energy")));
        }
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        super.write(tag);
        tag.putInt("fuel", fuelLevel);
        tag.putInt("initialFuelLevel", initialFuelLevel);

        itemHandler.ifPresent(item -> {
            CompoundNBT itemCompound = ((INBTSerializable<CompoundNBT>)item).serializeNBT();
            tag.put("inv", itemCompound);
        });

        energyHandler.ifPresent(h -> {
            tag.putInt("energy", h.getEnergyStored());
        });

        return tag;
    }

    private static boolean itemsValidator(@Nonnull ItemStack stack) {
        return true;
    }

    public int getMaxEnergyStored() {
        return DEFAULT_MAX_ENERGY;
    }

    public int getMaxTransferRate() {
        return DEFAULT_TRANSFER_RATE;
    }

    public int getSlotNum() {
        return this.SlotNum;
    };

    @Override
    public ITextComponent getDisplayName() {
        return new StringTextComponent(RegisteredObject.SHRUNK_SPACE_BUILDER.toString());
    }


    private ItemStackHandler createHandler() {
        ItemStackHandler stackHandler = new ItemStackHandler(getSlotNum()) {
                @Override
                public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                    return itemsValidator(stack);
                }

                @Nonnull
                @Override
                public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                    if(itemsValidator(stack) != true) {
                        return stack;
                    }
                    return super.insertItem(slot, stack, simulate);
                }
            };
        return stackHandler;
    }

    private MachineEnergyStorage createEnergy() {
        return new MachineEnergyStorage(getMaxEnergyStored(), getMaxTransferRate());
    }


    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> capability, @Nullable Direction side) {
        if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return itemHandler.cast();
        }
        if (capability == CapabilityEnergy.ENERGY) {
            return energyHandler.cast();
        }
        return super.getCapability(capability, side);
    }


    @Nullable
    @Override
    public abstract Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity);

    public int getProcessingTime() {
        return this.processingTime;
    }

    public void setProcessingTime(int value) {
        this.processingTime = value;
    }

    public int getInitialProcessingTime() {
        return this.initialProcessingTime;
    }

    public void setInitialProcessingTime(int value) {
        this.initialProcessingTime = value;
    }
}
