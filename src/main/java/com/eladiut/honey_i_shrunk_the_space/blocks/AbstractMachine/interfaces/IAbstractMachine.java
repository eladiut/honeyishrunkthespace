package com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public interface IAbstractMachine {
    TileEntity createTileEntity(BlockState state, IBlockReader world);
}
