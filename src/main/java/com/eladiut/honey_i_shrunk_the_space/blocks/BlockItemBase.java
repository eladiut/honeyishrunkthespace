package com.eladiut.honey_i_shrunk_the_space.blocks;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class BlockItemBase extends BlockItem {

    public BlockItemBase(Block blockIn) {
        super(blockIn, new Item.Properties().group(HoneyIShrunkTheSpace.TAB));
    }
}
