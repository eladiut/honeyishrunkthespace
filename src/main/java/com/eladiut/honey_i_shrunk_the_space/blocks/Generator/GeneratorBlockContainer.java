package com.eladiut.honey_i_shrunk_the_space.blocks.Generator;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineContainer;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderTileEntity;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers.InputSlotItemHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nonnull;

public class GeneratorBlockContainer extends AbstractMachineContainer {

    public GeneratorBlockContainer(int windowId, BlockPos pos, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        this(windowId, new GeneratorBlockTileEntity(), pos, playerInventory, playerEntity);
        super.Init(playerInventory);
    }

    public GeneratorBlockContainer(int windowId, GeneratorBlockTileEntity tileEntity, BlockPos pos, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        super(
                windowId,
                tileEntity,
                pos,
                playerEntity,
                RegistryHandler.GENERATOR_BLOCK_CONTAINER.get(),
                RegisteredObject.GENERATOR_BLOCK);
        super.Init(playerInventory);

    }

    @Override
    protected void addMachineSlots(TileEntity tileEntity) {
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
            slotsMap.put(GeneratorBlockSlots.INPUT.toString(), new InputSlotItemHandler(h, GeneratorBlockSlots.INPUT));
            slotsMap.forEach((key, value) -> {
                addSlot(value);
            });
        });
    }

    @Override
    protected InvWrapper getPlayerInventory(PlayerInventory playerInventory) {
        return new InvWrapper(playerInventory) {
            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
//                if(stack != playerInventory.getStackInSlot(slot) && slot == GeneratorBlockSlots.OUTPUT.getSlotNum()) {
//                    return stack;
//                }
                return super.insertItem(slot, stack, simulate);
            }
        };
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack = slot.getStack();
            itemstack = stack.copy();
            if (index == 0 || index == 1) {
                if (!this.mergeItemStack(stack, 2, 37, true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(stack, itemstack);
            } else {
                if (ForgeHooks.getBurnTime(stack) > 0 && !this.mergeItemStack(stack, 0, 1, false)) {
                    return ItemStack.EMPTY;
                } else if (index < 28 && !this.mergeItemStack(stack, 28, 37, false)) {
                    return ItemStack.EMPTY;
                } else if (index < 37 && !this.mergeItemStack(stack, 1, 28, false)) {
                    return ItemStack.EMPTY;
                }
            }

            if (stack.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }

            if (stack.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, stack);
        }

        return itemstack;
    }
}
