package com.eladiut.honey_i_shrunk_the_space.blocks.Generator;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;

public enum GeneratorBlockSlots implements MachineSlotsEnum {
    INPUT("input",0, 80, 34);

    private final String text;
    private final int slotNum;
    private final int xPosition;
    private final int yPosition;

    GeneratorBlockSlots(String name, int slot, int xPosition, int yPosition) {
        this.text = name;
        this.slotNum = slot;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    };

    @Override
    public String toString() {
        return text;
    }

    @Override
    public int getSlotNum() { return this.slotNum; }

    @Override
    public int getxPosition() {
        return xPosition;
    }

    @Override
    public int getyPosition() {
        return yPosition;
    }

}
