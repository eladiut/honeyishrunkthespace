package com.eladiut.honey_i_shrunk_the_space.blocks.Generator;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineGuiTile;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineScreen;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import org.lwjgl.opengl.GL11;

public class GeneratorBlockScreen extends AbstractMachineScreen<GeneratorBlockContainer> {
    private ResourceLocation GUI_TEXTURE = new ResourceLocation(HoneyIShrunkTheSpace.MOD_ID, "textures/gui/generator_gui.png");

    public GeneratorBlockScreen(GeneratorBlockContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        int relX = (this.width - this.xSize) / 2;
        int relY = (this.height - this.ySize) / 2;
        Minecraft.getInstance().fontRenderer.drawString("Generator", 10,10,4210752);
    }

    @Override
    protected ResourceLocation getGuiTexture() {
        return GUI_TEXTURE;
    }

    @Override
    protected boolean shouldDrawFlame() {
        return true;
    }

    @Override
    protected boolean shouldDrawEnergyBar() {
        return true;
    }

    @Override
    protected boolean shouldDrawShrinkerArrow() {
        return false;
    }

    @Override
    protected AbstractMachineGuiTile flameUnlitTile() {
        return AbstractMachineGuiTile.GENERATOR_UNLIT_FIRE;
    }


}
