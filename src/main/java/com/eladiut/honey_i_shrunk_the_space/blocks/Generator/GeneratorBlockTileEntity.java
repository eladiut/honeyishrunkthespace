package com.eladiut.honey_i_shrunk_the_space.blocks.Generator;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachineTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.IAbstractMachineTileEntity;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.MachineEnergyStorage;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.energy.CapabilityEnergy;

import javax.annotation.Nullable;

import java.util.concurrent.atomic.AtomicInteger;

import static com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler.GENERATOR_TILE_ENTITY;

public class GeneratorBlockTileEntity extends AbstractMachineTileEntity implements IAbstractMachineTileEntity {
    public GeneratorBlockTileEntity() {
        super(GENERATOR_TILE_ENTITY.get(), 1);
    }

    @Override
    public void Tick() {
        if (world.isRemote) {
            return;
        }

        BlockState blockState = world.getBlockState(pos);
        if (blockState.get(BlockStateProperties.LIT) != getFuelLevel() > 0) {
            world.setBlockState(pos, blockState.with(BlockStateProperties.LIT, getFuelLevel() > 0), 3);
        }

        this.PowerAdjacent(world, pos);

        if (getFuelLevel() > 0) {
            setFuelLevel(getFuelLevel()-1);
            getEnergyHandler().ifPresent(h -> {
                if(h.getEnergy() < getMaxEnergyStored()) {
                    h.receiveEnergy(60, false);
                    this.markDirty();
                }
            });
        } else {
            getItemHandler().ifPresent(h -> {
                ItemStack stack = h.getStackInSlot(GeneratorBlockSlots.INPUT.getSlotNum());
                int BurnTime = ForgeHooks.getBurnTime(stack);
                if (BurnTime > 0) {
                    h.extractItem(0, 1, false);
                    setFuelLevel(ForgeHooks.getBurnTime(stack));
                    setInitialFuelLevel(getFuelLevel());
                    this.markDirty();
                }
            });

        }
    }


    public void PowerAdjacent(World world, BlockPos pos) {
        if(world != null) {
            getEnergyHandler().ifPresent(energy -> {
                AtomicInteger capacity = new AtomicInteger(energy.getEnergyStored());
                if(capacity.get() > 0) {
                    for (Direction direction: Direction.values()) {
                        TileEntity te = world.getTileEntity(pos.offset(direction));
                        if(te != null) {
                            boolean doContinue = te.getCapability(CapabilityEnergy.ENERGY, direction).map(consumer -> {
                                if(consumer.canReceive()) {
                                    int receivedEnergy = consumer.receiveEnergy(20, false);
                                    capacity.addAndGet(-receivedEnergy);
                                    energy.extractEnergy(receivedEnergy, false);
                                    markDirty();
                                    return capacity.get() > 0;
                                }else {
                                    return true;
                                }
                            }).orElse(true);
                            if(!doContinue) {
                                return;
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void read(CompoundNBT tag) {
        super.read(tag);
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        return super.write(tag);
    }

    @Override
    public int getSlotNum() {
        return 1;
    }

    @Override
    public ITextComponent getDisplayName() {
        return new StringTextComponent(RegisteredObject.GENERATOR_BLOCK.toString());
    }

    @Nullable
    @Override
    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
            return new GeneratorBlockContainer(windowId, this, pos, playerInventory, playerEntity);
    }

}
