package com.eladiut.honey_i_shrunk_the_space.blocks.Generator;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.AbstractMachine;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

public class GeneratorBlock extends AbstractMachine {

    public GeneratorBlock() {
        super(
            Properties
                .create(Material.IRON)
                .lightValue(6)
        );
    }

    @Override
    public BooleanProperty propertyToUseAsLighting() {
        return BlockStateProperties.LIT;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new GeneratorBlockTileEntity();
    }


}
