package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

public interface IWallBlock {
     int getWallValue();
}
