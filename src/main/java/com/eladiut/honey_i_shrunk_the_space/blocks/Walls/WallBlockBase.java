package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.ToolType;

public abstract class WallBlockBase extends Block {
    private boolean hasEntered = false;
    public Vec3d LinkedWorldLocation = new Vec3d(0d,0d,0d);

    public WallBlockBase(Properties properties) { super(properties); }

    public boolean isHasEntered() {
        return hasEntered;
    }

    public abstract int getWallValue();
}
