package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.ToolType;

public class WallBlock extends WallBlockBase implements IWallBlock {
    private boolean hasEntered = false;
    public Vec3d LinkedWorldLocation = new Vec3d(0d,0d,0d);

    public WallBlock() {
        super(
                Block.Properties.create(Material.ROCK)
                .hardnessAndResistance(6f,24f)
                .sound(SoundType.STONE)
                .harvestLevel(1)
                .harvestTool(ToolType.PICKAXE));

    }
    public boolean isHasEntered() {
        return hasEntered;
    }

    @Override
    public int getWallValue() {
        return 1;
    }
}
