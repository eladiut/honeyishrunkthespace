package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

public enum WallBlockValue {
    DEFAULT(0),
    WALL_BLOCK(1),
    COMPRESSED_WALL_BLOCK(2),
    DBL_COMPRESSED_WALL_BLOCK(3),
    TRPL_COMPRESSED_WALL_BLOCK(4),
    QDRPL_COMPRESSED_WALL_BLOCK(5);

    int value;
    WallBlockValue(int i) {
        this.value = i;
    }
}
