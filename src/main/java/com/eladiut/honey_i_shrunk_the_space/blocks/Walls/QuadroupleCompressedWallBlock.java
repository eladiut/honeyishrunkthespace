package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class QuadroupleCompressedWallBlock extends WallBlockBase implements IWallBlock {

    public QuadroupleCompressedWallBlock() {
        super(
                Properties.create(Material.ROCK)
                        .hardnessAndResistance(6f,24f)
                .sound(SoundType.STONE)
                .harvestLevel(1)
                .harvestTool(ToolType.PICKAXE));
    }

    @Override
    public int getWallValue() {
        return 4;
    }
}
