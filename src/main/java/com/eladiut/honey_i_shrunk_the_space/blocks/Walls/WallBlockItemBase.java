package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;

public class WallBlockItemBase extends BlockItem {
    private static int wallValue;
    public WallBlockItemBase(WallBlockBase blockIn) {
        super(blockIn, new Properties().group(HoneyIShrunkTheSpace.TAB));
        this.wallValue = blockIn.getWallValue();
    }

    public int getWallValue() {
        return this.wallValue;
    }
}
