package com.eladiut.honey_i_shrunk_the_space.blocks.Walls;

import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import net.minecraft.item.Item;

public class WallBlockHandler {
    public static int getWallValue(Item item) {
        if(item == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.WALL_BLOCK)) {
            return WallBlockValue.WALL_BLOCK.value;
        } else if(item == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.COMPRESSED_WALL_BLOCK)) {
            return WallBlockValue.COMPRESSED_WALL_BLOCK.value;
        } else if(item == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.DBL_COMPRESSED_WALL_BLOCK)) {
            return WallBlockValue.DBL_COMPRESSED_WALL_BLOCK.value;
        } else if(item == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.TRPL_COMPRESSED_WALL_BLOCK)) {
            return WallBlockValue.TRPL_COMPRESSED_WALL_BLOCK.value;
        } else if(item == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.QDRPL_COMPRESSED_WALL_BLOCK)) {
            return WallBlockValue.QDRPL_COMPRESSED_WALL_BLOCK.value;
        } else {
            return WallBlockValue.DEFAULT.value;
        }
    }
}
