package com.eladiut.honey_i_shrunk_the_space.setup;

import com.eladiut.honey_i_shrunk_the_space.blocks.Generator.GeneratorBlockScreen;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public class ClientProxy implements IProxy {
    public void Init() {
        ScreenManager.registerFactory(RegistryHandler.SHRUNK_SPACE_BUILDER_CONTAINER.get(), ShrunkSpaceBuilderScreen::new);
        ScreenManager.registerFactory(RegistryHandler.GENERATOR_BLOCK_CONTAINER.get(), GeneratorBlockScreen::new);
    }

    @Override
    public World getClientWorld() {
        return Minecraft.getInstance().world;
    }

    @Override
    public PlayerEntity getClientPlayer() {
        return Minecraft.getInstance().player;
    }
}
