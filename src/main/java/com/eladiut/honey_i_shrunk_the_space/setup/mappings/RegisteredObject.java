package com.eladiut.honey_i_shrunk_the_space.setup.mappings;

public enum RegisteredObject {
    NONE(""),
    COMPRESSED_DIMENSION("compressed_dimension"),
    WALL_BLOCK("wall_block"),
    COMPRESSED_WALL_BLOCK("compressed_wall_block"),
    DBL_COMPRESSED_WALL_BLOCK("dblcompressed_wall_block"),
    TRPL_COMPRESSED_WALL_BLOCK("trplcompressed_wall_block"),
    QDRPL_COMPRESSED_WALL_BLOCK("qdrplcompressed_wall_block"),
    SHRUNK_SPACE("shrunk_space"),
    TELEPORTER("teleporter"),
    ENDER_SHARD("ender_shard"),
    GAVEL("gavel"),
    SHRUNK_SPACE_BUILDER_TILE_ENTITY("shrunk_space_builder_tile_entity"),
    SHRUNK_SPACE_BUILDER("shrunk_space_builder"),
    SHRUNK_SPACE_BUILDER_CONTAINER("shrunk_space_builder_container"),
    GENERATOR_BLOCK("generator"),
    GENERATOR_BLOCK_TILE_ENTITY("generator_tile_entity"),
    GENERATOR_BLOCK_CONTAINER("generator_container");

    private final String text;

    /**
     * @param text
     */
    RegisteredObject(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
