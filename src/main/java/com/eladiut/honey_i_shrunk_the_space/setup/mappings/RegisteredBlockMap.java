package com.eladiut.honey_i_shrunk_the_space.setup.mappings;

import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import java.util.HashMap;

public class RegisteredBlockMap extends HashMap<String, RegistryObject<Block>> {
    public Block getBlock(RegisteredObject key) {
        if(key != null) {
            if( super.get(key.toString()) != null ) {
                return super.get(key.toString()).get();
            }
        }
        System.out.println("BLOCK" + key + "NO FOUND !");
        return null;
    }
}
