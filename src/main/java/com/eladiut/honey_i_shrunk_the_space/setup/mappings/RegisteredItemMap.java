package com.eladiut.honey_i_shrunk_the_space.setup.mappings;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;

import java.util.HashMap;

public class RegisteredItemMap extends HashMap<String, RegistryObject<Item>> {
    public Item getItem(RegisteredObject key) {
        return super.get(key.toString()).get();
    }
}
