package com.eladiut.honey_i_shrunk_the_space.setup;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public interface IProxy {
    World getClientWorld();
    PlayerEntity getClientPlayer();

    void Init();
}
