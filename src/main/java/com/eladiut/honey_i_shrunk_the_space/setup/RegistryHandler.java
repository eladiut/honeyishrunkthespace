package com.eladiut.honey_i_shrunk_the_space.setup;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import com.eladiut.honey_i_shrunk_the_space.blocks.*;
import com.eladiut.honey_i_shrunk_the_space.blocks.Generator.GeneratorBlock;
import com.eladiut.honey_i_shrunk_the_space.blocks.Generator.GeneratorBlockContainer;
import com.eladiut.honey_i_shrunk_the_space.blocks.Generator.GeneratorBlockTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilder;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderContainer;
import com.eladiut.honey_i_shrunk_the_space.blocks.ShrunkSpaceBuilder.ShrunkSpaceBuilderTileEntity;
import com.eladiut.honey_i_shrunk_the_space.blocks.Walls.*;
import com.eladiut.honey_i_shrunk_the_space.dimensions.CompressedModDimension;
import com.eladiut.honey_i_shrunk_the_space.items.EnderShard;
import com.eladiut.honey_i_shrunk_the_space.items.Gavel;
import com.eladiut.honey_i_shrunk_the_space.items.TeleporterItem;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredBlockMap;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredItemMap;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.util.LinkedWorldLocationHandler;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class RegistryHandler {

    public static final DeferredRegister<Item> ITEMS = new DeferredRegister<Item>(ForgeRegistries.ITEMS, HoneyIShrunkTheSpace.MOD_ID);
    public static final DeferredRegister<Block> BLOCKS = new DeferredRegister<Block>(ForgeRegistries.BLOCKS, HoneyIShrunkTheSpace.MOD_ID);
    public static final DeferredRegister<ModDimension> DIMENSIONS = new DeferredRegister<ModDimension>(ForgeRegistries.MOD_DIMENSIONS, HoneyIShrunkTheSpace.MOD_ID);
    public static final DeferredRegister<ContainerType<?>> CONTAINERS = new DeferredRegister<ContainerType<?>>(ForgeRegistries.CONTAINERS, HoneyIShrunkTheSpace.MOD_ID);
    public static final DeferredRegister<TileEntityType<?>> TILE_ENTITIES = new DeferredRegister<>(ForgeRegistries.TILE_ENTITIES, HoneyIShrunkTheSpace.MOD_ID);

    public static final LinkedWorldLocationHandler LINKED_WORLD_LOCATION_HANDLER = new LinkedWorldLocationHandler();

    public static void init() {
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        DIMENSIONS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TILE_ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
        CONTAINERS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }


    ////////////////////////// Dimensions ////////////////////

    public static final RegistryObject<ModDimension> COMPRESSED_DIMENSION = DIMENSIONS.register(RegisteredObject.COMPRESSED_DIMENSION.toString(), () -> new CompressedModDimension());

    ///////////////////////// Blocks /////////////////////////

    // Compact Machine Walls

    public static RegisteredBlockMap BLOCK_SET = new RegisteredBlockMap();
    public static RegisteredItemMap BLOCK_ITEM_SET = new RegisteredItemMap();
    static {
        BLOCK_SET.put(RegisteredObject.WALL_BLOCK.toString(), BLOCKS.register(RegisteredObject.WALL_BLOCK.toString(), WallBlock::new));
        BLOCK_SET.put(RegisteredObject.COMPRESSED_WALL_BLOCK.toString(), BLOCKS.register(RegisteredObject.COMPRESSED_WALL_BLOCK.toString(), CompressedWallBlock::new));
        BLOCK_SET.put(RegisteredObject.DBL_COMPRESSED_WALL_BLOCK.toString(), BLOCKS.register(RegisteredObject.DBL_COMPRESSED_WALL_BLOCK.toString(), DoubleCompressedWallBlock::new));
        BLOCK_SET.put(RegisteredObject.TRPL_COMPRESSED_WALL_BLOCK.toString(), BLOCKS.register(RegisteredObject.TRPL_COMPRESSED_WALL_BLOCK.toString(), TripleCompressedWallBlock::new));
        BLOCK_SET.put(RegisteredObject.QDRPL_COMPRESSED_WALL_BLOCK.toString(), BLOCKS.register(RegisteredObject.QDRPL_COMPRESSED_WALL_BLOCK.toString(), QuadroupleCompressedWallBlock::new));
        BLOCK_SET.put(RegisteredObject.SHRUNK_SPACE_BUILDER.toString(), BLOCKS.register(RegisteredObject.SHRUNK_SPACE_BUILDER.toString(), ShrunkSpaceBuilder::new));
        BLOCK_SET.put(RegisteredObject.GENERATOR_BLOCK.toString(), BLOCKS.register(RegisteredObject.GENERATOR_BLOCK.toString(), GeneratorBlock::new));
        BLOCK_SET.put(RegisteredObject.SHRUNK_SPACE.toString(), BLOCKS.register(RegisteredObject.SHRUNK_SPACE.toString(), ShrunkSpaceBase::new));

        BLOCK_SET.forEach((name, block) -> BLOCK_ITEM_SET.put(name, ITEMS.register(name, () -> new BlockItemBase(block.get()))));
    }

    ///////////////////////// Tiles /////////////////////////

    public static final RegistryObject<TileEntityType<ShrunkSpaceBuilderTileEntity>> SHRUNK_SPACE_BUILDER_TILE_ENTITY = TILE_ENTITIES.register(RegisteredObject.SHRUNK_SPACE_BUILDER_TILE_ENTITY.toString(), () -> TileEntityType.Builder.create(ShrunkSpaceBuilderTileEntity::new, BLOCK_SET.getBlock(RegisteredObject.SHRUNK_SPACE_BUILDER)).build(null));
    public static final RegistryObject<TileEntityType<GeneratorBlockTileEntity>> GENERATOR_TILE_ENTITY = TILE_ENTITIES.register(RegisteredObject.GENERATOR_BLOCK_TILE_ENTITY.toString(), () -> TileEntityType.Builder.create(GeneratorBlockTileEntity::new, BLOCK_SET.getBlock(RegisteredObject.GENERATOR_BLOCK)).build(null));

    ///////////////////////// Items /////////////////////////

    public static final RegistryObject<Item> TELEPORTER = ITEMS.register(RegisteredObject.TELEPORTER.toString(), TeleporterItem::new);
    public static final RegistryObject<Item> ENDER_SHARD = ITEMS.register(RegisteredObject.ENDER_SHARD.toString(), EnderShard::new);
    public static final RegistryObject<Item> GAVEL = ITEMS.register(RegisteredObject.GAVEL.toString(), Gavel::new);

    /////////////////////// Containers /////////////////////

    public static final RegistryObject<ContainerType<ShrunkSpaceBuilderContainer>> SHRUNK_SPACE_BUILDER_CONTAINER = CONTAINERS.register(
            RegisteredObject.SHRUNK_SPACE_BUILDER_CONTAINER.toString(),
            () -> IForgeContainerType.create(((windowId, inv, data) -> {
                BlockPos blockPos = data.readBlockPos();
                return new ShrunkSpaceBuilderContainer(windowId, blockPos, inv, HoneyIShrunkTheSpace.proxy.getClientPlayer());
            }))
    );

    public static final RegistryObject<ContainerType<GeneratorBlockContainer>> GENERATOR_BLOCK_CONTAINER = CONTAINERS.register(
            RegisteredObject.GENERATOR_BLOCK_CONTAINER.toString(),
            () -> IForgeContainerType.create(((windowId, inv, data) -> {
                BlockPos blockPos = data.readBlockPos();
                return new GeneratorBlockContainer(windowId, blockPos, inv, HoneyIShrunkTheSpace.proxy.getClientPlayer());
            }))
    );

}
