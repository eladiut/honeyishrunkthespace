package com.eladiut.honey_i_shrunk_the_space.dimensions;

import net.minecraft.world.gen.FlatGenerationSettings;

public class CompressedGenSettings extends FlatGenerationSettings {
    @Override
    public int getStrongholdCount() {
        return 0;
    }

    public int getBiomeSize() {
        return 0;
    }
    public int getRiverSize() {
        return 0;
    }
    public int getBiomeId() {
        return -1;
    }

    @Override
    public int getBedrockRoofHeight() {
        return 256;
    }

    @Override
    public int getBedrockFloorHeight() {
        return 0;
    }
}
