package com.eladiut.honey_i_shrunk_the_space.dimensions;

import com.google.common.collect.ImmutableSet;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.BiomeProvider;

import java.util.Set;

public class CompressedBiomeProvider extends BiomeProvider {
    private static final Set<Biome> BiomeList = ImmutableSet.of(Biomes.THE_VOID);
    public CompressedBiomeProvider() {
        super(BiomeList);
    }

    @Override
    public Biome getNoiseBiome(int x, int y, int z) {
        return Biomes.THE_VOID;
    }
}
