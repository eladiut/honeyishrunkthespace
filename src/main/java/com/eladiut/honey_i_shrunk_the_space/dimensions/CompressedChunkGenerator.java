package com.eladiut.honey_i_shrunk_the_space.dimensions;

import net.minecraft.world.IWorld;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.FlatChunkGenerator;
import net.minecraft.world.gen.FlatGenerationSettings;

public class CompressedChunkGenerator extends FlatChunkGenerator {
    public CompressedChunkGenerator(IWorld worldIn, BiomeProvider biomeProviderIn, FlatGenerationSettings settingsIn) {
        super(worldIn, biomeProviderIn, settingsIn);
    }
}
