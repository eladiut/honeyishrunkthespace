package com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public abstract class OutputSlotItemHandler extends SlotItemHandler implements IOutputSlotItemHandler {
    public OutputSlotItemHandler(IItemHandler itemHandler, MachineSlotsEnum slot) {
        super(itemHandler, slot.getSlotNum(), slot.getxPosition(), slot.getyPosition());
    }

    @Override
    public abstract Item getOutputItem();

    @Override
    public boolean isItemValid(@Nonnull ItemStack stack) {
        if (stack.isEmpty())
            return false;
        if (getOutputItem() == null)
            return true;
        return stack.getItem() == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.SHRUNK_SPACE);
    }
}
