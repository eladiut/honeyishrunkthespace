package com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;
import com.eladiut.honey_i_shrunk_the_space.setup.mappings.RegisteredObject;
import com.eladiut.honey_i_shrunk_the_space.setup.RegistryHandler;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nonnull;

public class WallSlotItemHandler extends InputSlotItemHandler {
    public WallSlotItemHandler(IItemHandler itemHandler, MachineSlotsEnum slot) {
        super(itemHandler, slot);
    }

    public static boolean isWallBlock(@Nonnull ItemStack stack) {
        Item stackItem = stack.getItem();
        return (stackItem == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.WALL_BLOCK)) ||
                (stackItem == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.COMPRESSED_WALL_BLOCK)) ||
                (stackItem == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.DBL_COMPRESSED_WALL_BLOCK)) ||
                (stackItem == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.TRPL_COMPRESSED_WALL_BLOCK)) ||
                (stackItem == RegistryHandler.BLOCK_ITEM_SET.getItem(RegisteredObject.QDRPL_COMPRESSED_WALL_BLOCK));
    }

    @Override
    public boolean isItemValid(@Nonnull ItemStack stack) {
        return this.isWallBlock(stack);
    }
}
