package com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public interface IOutputSlotItemHandler {
    Item getOutputItem();
    boolean isItemValid(@Nonnull ItemStack stack);
}
