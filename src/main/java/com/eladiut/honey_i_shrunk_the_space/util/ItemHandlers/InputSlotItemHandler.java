package com.eladiut.honey_i_shrunk_the_space.util.ItemHandlers;

import com.eladiut.honey_i_shrunk_the_space.blocks.AbstractMachine.interfaces.MachineSlotsEnum;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class InputSlotItemHandler extends SlotItemHandler {
    public InputSlotItemHandler(IItemHandler itemHandler, MachineSlotsEnum slot) {
        super(itemHandler, slot.getSlotNum(), slot.getxPosition(), slot.getyPosition());
    }
}
