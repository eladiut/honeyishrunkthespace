package com.eladiut.honey_i_shrunk_the_space.util;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraftforge.common.util.INBTSerializable;

public class MyVector3D implements INBTSerializable<CompoundNBT> {
    private int x;
    private int y;
    private int z;

    public MyVector3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getZ() {
        return z;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public void setZ(int z) {
        this.z = z;
    }

    public double getVolume() {
        return this.x * this.y * this.z;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putInt("xSize", this.x);
        nbt.putInt("ySize", this.y);
        nbt.putInt("zSize", this.z);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.x = nbt.getInt("xSize");
        this.y = nbt.getInt("ySize");
        this.z = nbt.getInt("zSize");
    }
}
