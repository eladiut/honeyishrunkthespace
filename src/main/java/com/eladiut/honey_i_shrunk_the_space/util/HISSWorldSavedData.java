package com.eladiut.honey_i_shrunk_the_space.util;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.storage.WorldSavedData;

public class HISSWorldSavedData extends WorldSavedData {
    private static final String DATA_NAME = HoneyIShrunkTheSpace.MOD_ID + "_Data";
    public static CompoundNBT DATA = null;
    // Required constructors

    public HISSWorldSavedData() {
        super(DATA_NAME);
    }

    public HISSWorldSavedData(String s) {
        super(s);
    }

    @Override
    public void read(CompoundNBT nbt) {

    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        return null;
    }

    // WorldSavedData methods
}
