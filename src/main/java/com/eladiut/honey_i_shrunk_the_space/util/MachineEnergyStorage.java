package com.eladiut.honey_i_shrunk_the_space.util;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.EnergyStorage;

public class MachineEnergyStorage extends EnergyStorage implements INBTSerializable<CompoundNBT> {
    public MachineEnergyStorage(int capacity, int maxTransfer) {
        super(capacity, maxTransfer);
    }



    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void addEnergy(int energy) {
        if(this.energy + energy <= getMaxEnergyStored()) {
            this.energy += energy;
        }
    }

    public int getEnergy() {
        return this.getEnergyStored();
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt("energy", getEnergyStored());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.energy = nbt.getInt("energy");
    }
}
