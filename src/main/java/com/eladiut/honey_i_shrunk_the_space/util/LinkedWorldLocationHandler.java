package com.eladiut.honey_i_shrunk_the_space.util;

import net.minecraft.util.math.Vec3d;

import java.util.HashSet;

public class LinkedWorldLocationHandler {
    public HashSet<Vec3d> LinkedWorldLocations;

    public LinkedWorldLocationHandler() {
        this.LinkedWorldLocations = new HashSet<Vec3d>();
    }
}
