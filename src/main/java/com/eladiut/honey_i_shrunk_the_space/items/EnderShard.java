package com.eladiut.honey_i_shrunk_the_space.items;

import net.minecraft.item.ItemStack;

public class EnderShard extends ItemBase{

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return 64;
    }
}
