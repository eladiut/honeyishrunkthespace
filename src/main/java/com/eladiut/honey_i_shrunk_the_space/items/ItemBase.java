package com.eladiut.honey_i_shrunk_the_space.items;

import com.eladiut.honey_i_shrunk_the_space.HoneyIShrunkTheSpace;
import net.minecraft.item.Item;

public class ItemBase extends Item {

    public ItemBase() {
        super(new Item.Properties().group(HoneyIShrunkTheSpace.TAB));
    }
}
