package com.eladiut.honey_i_shrunk_the_space.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Gavel extends ItemBase {
    public static final int MAX_DURABILITY = 1000;

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return 1;
    }

    @Override
    public boolean hasContainerItem(ItemStack stack) {
        return getDamage(stack) < getMaxDamage(stack);
    }

    @Override
    public ItemStack getContainerItem(ItemStack itemStack) {
        itemStack.attemptDamageItem(10, Item.random, null);
        if(itemStack.getDamage() <= 0) {
            return itemStack;
        } else {
            final ItemStack newItemStack = itemStack.copy();
            newItemStack.attemptDamageItem(10, Item.random, null);
            return newItemStack;
        }
    }

    @Override
    public boolean showDurabilityBar(ItemStack stack) {
        return isDamaged(stack);
    }

    @Override
    public boolean isDamageable() {
        return true;
    }

    @Override
    public int getMaxDamage(ItemStack stack) {
        return MAX_DURABILITY;
    }
}
