package com.eladiut.honey_i_shrunk_the_space.items;

import net.minecraft.item.ItemStack;

public class TeleporterItem extends ItemBase{
    public static int durability = 1000;
    @Override
    public int getItemStackLimit(ItemStack stack) {
        return 1;
    }

}
